# 동네 친구 찾기 API
## GPS(위도, 경도) 기술을 사용하여 동네 친구를 찾을 수 있게 하는 서비스의 API입니다.

### 사용기술
```
- java 17
- JPA
- Postgres (+Procedure)
```

### 기능
```
1. kakao API를 이용한 주소 - 좌표 변환
2. kakao API를 이용한 좌표 - 주소 변환
```

### 필요 프로시저
```
CREATE OR REPLACE FUNCTION public.get_near_friends(positionX DOUBLE PRECISION, positionY DOUBLE PRECISION, distance DOUBLE PRECISION)
RETURNS TABLE
(
    nickname varchar
    , hobby varchar
    , gender varchar
    , distance_m double precision
)
as
$$
DECLARE
    v_record RECORD;
BEGIN
    for v_record in (
        select
            member.nickname, member.hobby, member.gender, earth_distance(ll_to_earth(member.posy, member.posx), ll_to_earth(positionY, positionX)) as distance_m
        from member
        where earth_distance(ll_to_earth(member.posy, member.posx), ll_to_earth(positionY, positionX)) <= distance
        order by distance_m asc
    )
    loop
        nickname := v_record.nickname;
        hobby := v_record.hobby;
        gender := v_record.gender;
        distance_m := v_record.distance_m;
        return next;
    end loop;
END;
$$
LANGUAGE plpgsql

```

> This is a first blockqute.
>	> This is a second blockqute.
>	>	> This is a third blockqute.
>